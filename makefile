
all: myElf

myElf: myElf.o
	gcc -g -Wall -o myElf myElf.o
myElf.o: myElf.c 
	gcc -g -Wall -c -o myElf.o myElf.c

#tell make that "clean" is not a file name!
.PHONY: clean

#Clean the build directory
clean: 
	rm -f *.o myElf

 
