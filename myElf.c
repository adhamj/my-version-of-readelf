#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "elf.h"

#define PRINT_OFF 50

/*====================(Func. Deff.)=================================*/
char* typeToString(int type);
void apply_printing_offset(char* str,int offset);
void examine_elf();
void print_section_headers();
void print_symbols();
void quit();
/*==================================================================
 =====================(Var. Deff.)==================================*/
void (*function[4])() = {examine_elf,print_section_headers,print_symbols,quit};
int i;
int current_fd = -1;
int num_of_section_headers;
void *map;
char user_input[16];
char file_name[101];
char elf_magic_numbers[4] = {0x7f,'E','L','F'};
struct stat fd_stat;
Elf64_Ehdr *header;
Elf64_Shdr *section;
Elf64_Sym *symbol;
Elf64_Sym *symbol_dynamic;
/*====================(Func. Deff.)=================================*/

int main(int argc,char** argv){

    while(1){
        printf("Choose action:\n1-Examine ELF File\n2-Print Section Headers\n3-Print Symbols\n4-Quit\n");
        fgets(user_input,16,stdin);
        sscanf(user_input,"%d\n",&i);
        function[i - 1]();
    }
    return 0;
}
/*==================(Func. Impl.)===================================*/
void quit(){
    exit(0);
}


void print_symbols(){

    section = map+header->e_shoff;
    Elf64_Shdr* str_tab = (void*)header+header->e_shoff+header->e_shstrndx*header->e_shentsize;
    int sym_num=0;
    int dyn_num=0;
    Elf64_Sym* symbol;
    Elf64_Sym* dyn_sym_tab;
    Elf64_Shdr* str_sym_tab;
    Elf64_Shdr* dyn_str_sym_tab;
    int shnum = header->e_shnum;
    char* name;
    
    for(int i=0; i < shnum ; i++){
        name = (void*) header+str_tab->sh_offset+section[i].sh_name;
        if(strcmp(".symtab", name) == 0){
            symbol = (void*)header + section[i].sh_offset;
            sym_num = (section[i].sh_size)/(section[i].sh_entsize);
        }
        else if(strcmp(".strtab", name) == 0){
            str_sym_tab = (void*)header + section[i].sh_offset;
        }
        else if(strcmp(".dynsym", name) == 0){
            dyn_sym_tab = (void*)header + section[i].sh_offset;
            dyn_num = (section[i].sh_size)/(section[i].sh_entsize);
        }
        else if(strcmp(".dynstr", name) == 0){
            dyn_str_sym_tab = (void*)header + section[i].sh_offset;
        }
    }
    
    
    int value;
    int section_idx;
    char* sec_name;
    char* sym_name;
    
    if(dyn_num > 0){
        printf("\t       *******************************************\n");
        printf("\t           ****** DYNAMIC SYMBOL TABLE ******\n");
        printf("\t       *******************************************\n");
        
        printf(" Index          Value          Sec-idx          Sec-name                Sym-name\n\n");
    
    
        for(int i=0; i<dyn_num; i++){
            sym_name = (char*)dyn_str_sym_tab+dyn_sym_tab[i].st_name;
            if(sym_name == 0)
                sym_name = "";
            value = dyn_sym_tab[i].st_value;
            section_idx = dyn_sym_tab[i].st_shndx;
            switch(section_idx){
                case 0: sec_name = "UNDEF"; break;
                case 0xff00: sec_name = "LORESERVE"; break;
                //case 0xff00: sec_name = "LOPROC"; break;
                //case 0xff00: sec_name = "BEFORE"; break;
                case 0xff01: sec_name = "AFTER"; break;
                case 0xff1f: sec_name = "HIPROC"; break;
                case 0xff20: sec_name = "LOOS"; break;
                case 0xff3f: sec_name = "HIOS"; break;
                case 0xfff1: sec_name = "ABS"; break;
                case 0xfff2: sec_name = "COMMON"; break;
                case 0xffff: sec_name = "XINDEX"; break;
                //case 0xffff: sec_name = "HIRESERVE"; break;
                default: sec_name = (void*)header+str_tab->sh_offset+section[section_idx].sh_name;
            }
            printf("[%d]\t\t%-9x\t%-10d\t%-20s\t%s\n", i, value, section_idx, sec_name, sym_name);
        }
    }
    
    
    
    
    
    
    
    printf("\t       *******************************************\n");
    printf("\t            ***********SYMBOL TABLE ************\n");
    printf("\t       *******************************************\n");
    printf(" Index          Value          Sec-idx          Sec-name                Sym-name\n\n");
    
    for(int i=0; i<sym_num; i++){
        sym_name = (char*)str_sym_tab+symbol[i].st_name;
        if(sym_name == 0)
            sym_name = "";
        value = symbol[i].st_value;
        section_idx = symbol[i].st_shndx;
        switch(section_idx){
            case 0: sec_name = "UNDEF"; break;
            case 0xff00: sec_name = "LORESERVE"; break;
            //case 0xff00: sec_name = "LOPROC"; break;
            //case 0xff00: sec_name = "BEFORE"; break;
            case 0xff01: sec_name = "AFTER"; break;
            case 0xff1f: sec_name = "HIPROC"; break;
            case 0xff20: sec_name = "LOOS"; break;
            case 0xff3f: sec_name = "HIOS"; break;
            case 0xfff1: sec_name = "ABS"; break;
            case 0xfff2: sec_name = "COMMON"; break;
            case 0xffff: sec_name = "XINDEX"; break;
            //case 0xffff: sec_name = "HIRESERVE"; break;
            default: sec_name = (void*)header+str_tab->sh_offset+section[section_idx].sh_name;
        }
        printf("[%d]\t\t%-9x\t%-10d\t%-20s\t%s\n", i, value, section_idx, sec_name, sym_name);
    }
}

void examine_elf(){
    printf("Insert File Name:\n");
    fgets(file_name,101,stdin);
    strtok(file_name,"\n");
    if(current_fd != -1){
        close(current_fd);
    }
    else if((current_fd = open(file_name,O_RDWR)) < 0){
        perror("error opening file");
        current_fd = -1;
        return;
    }
    if( fstat(current_fd, &fd_stat) != 0 ) {
        perror("stat failed");
        return;
    }
    if(map){
        munmap(map, fd_stat.st_size);
    }
    map = mmap(0, fd_stat.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, current_fd, 0);
    if (map == MAP_FAILED) {
	perror("error in mmap()");
	return;
    }
    header = (Elf64_Ehdr *) map;
/*todo 1 :Print Bytes 1,2,3 of the magic number (in ASCII).
* Henceforth, you should check that the number is consistent with an ELF file,
* and refuse to continue if it is not. 
*/
    for(int j=0 ; j<4 ; j++){
         if(header->e_ident[j] != elf_magic_numbers[j]){
             perror("wrong magic numbers\n");
             return;
        }
    }
    printf("\n");
    apply_printing_offset("  1-current magic numbers:",PRINT_OFF);
    for(int j=1 ; j<4 ; j++){
         printf("%02x ",header->e_ident[j]);
    }
    printf("\n");
    
/*todo 2:Print The data encoding scheme of the object file. */
    apply_printing_offset("  2-Data encoding scheme:",PRINT_OFF);
    switch(header->e_ident[EI_DATA]){
        case 2:
            printf("Big Endian\n");
            break;
        case 1:
            printf("Little Endian\n");
            break;
    }
    
/*todo 3:Print Entry point*/
    apply_printing_offset("  3-Entry point:",PRINT_OFF);
    printf("0x%lX\n",header->e_entry);
    
/*todo 4: print The file offset in which the section header table resides. */
    apply_printing_offset("  3-Section header offset:",PRINT_OFF);
    printf("0x%lX\n",header->e_shoff);
    
/*todo 5:Print The number of section header entries. */
    apply_printing_offset("  4-Number of section header entries:",PRINT_OFF);
    printf("%d\n",header->e_shnum);
    
/*todo6:Print The size of section header entries. */
    apply_printing_offset("  6-Size of section header entries:",PRINT_OFF);
    printf("%d\n",header->e_shentsize);
    
/*todo7:Print print The file offset in which the program header table resides. */
    apply_printing_offset("  7-Program header offset:",PRINT_OFF);
    printf("0x%lX\n",header->e_phoff);
    
/*todo8:Print print The number of program header entries.  */
    apply_printing_offset("  4-Number of program header entries:",PRINT_OFF);
    printf("%d\n",header->e_phnum);
    
/*todo9:Print The size of program header entries. */
    apply_printing_offset("  9-Size of program header entries:",PRINT_OFF);
    printf("%d\n",header->e_phentsize);    
    
    
    printf("\n");
}

void print_section_headers(){
    if(current_fd < 0){
        printf("\n  invalid file\n\n");
        return;
    }

    section = (Elf64_Shdr *)(map + header->e_shoff);
    int shnum = header->e_shnum;
    Elf64_Shdr *sh_strtab =  section + header->e_shstrndx;
    const char *const names = map + sh_strtab->sh_offset;
    
    printf(" Index       Name                Adress");
    printf("        Offset        Size     Type \n\n");
    
    for (int i = 0; i < shnum; ++i) {
        char* typeStr = typeToString(section[i].sh_type);
        printf("  [%2d]  ",i);
        printf("   %-17s",&names[section[i].sh_name]);
        printf("    0x%08lX  ",section[i].sh_addr);
        printf("    0x%04lX  ",section[i].sh_offset);
        printf("      %-9ld",section[i].sh_size);
        printf("%s\n\n",typeStr);
    }

}

void apply_printing_offset(char* str,int offset){
    printf("%s%*s",str,(int)(strlen(str) - offset),"");
}

char* typeToString(int type){
    if(type == 0)
        return "NULL";
    else if(type == 1)
        return "PROGBITS";
    else if(type == 2)
        return "SYMTAB";
    else if(type == 3)
        return "STRTAB";
    else if(type == 4)
        return "RELA";
    else if(type == 5)
        return "HASH";
    else if(type == 6)
        return "DYNAMIC";
    else if(type == 7)
        return "NOTE";
    else if(type == 8)
        return "NOBITS";
    else if(type == 9)
        return "REL";
    else if(type == 10)
        return "SHLIB";
    else if(type == 11)
        return "DYNSYM";
    else if(type == 0x60000000)
        return "LOOS";
    else if(type == 0x6FFFFFFF)
        return "HIOS";
    else if(type == 0x70000000)
        return "LOPROC";
    else if(type == 0x7FFFFFFF)
        return "HIPROC";
    else if(type == 1879048182)
        return "HASH";
    else if(type == 1879048190)
        return "VERNEED";
    else if(type == 14)
        return "INIT_ARRAY";
    else if(type == 15)
        return "FINI_ARRAY";
    else return "";
}
